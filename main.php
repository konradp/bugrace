<?php
  include_once("checklogged.php");
  session_start();
  if( !checklogged() ) {
    header("Location: index.php");
    exit;
  }
  ?>

<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"></head>
<body>

<!-- HEADER AND LOGOUT BUTTON -->
<div align="right">
<?php
// QUERY
  require_once("Db.php");
  $db_handle = new Db(); 
  $query = "
    SELECT id, name
    FROM User
    WHERE id=".$_SESSION['userid']."
  ";
  $users = $db_handle->select($query); 
  $count = count($users);
  $username = $users[0]['name'];
  $user_id = $_SESSION['userid'];
  echo "Hello, ".$username.": ";
?>
<a href="logout.php"><button name="logout">Logout</button></a>
</div>
<hr>

<!-- MAIN CONTENT -->
<!-- The whole table is a form, since a table row cannot be a form -->
<form action="addBugDB.php" method="POST">
<table>
<!-- TITLES -->
<tr>
  <td>
    <h3>Top scores</h3>
  </td>
  <td align="center">
    <h3>Distribution</h3>
  </td>
  <td>
    <h3>Submit a new bug</h3>
  </td>
</tr>

<!-- CONTENT -->
<tr>
  <!-- SCORE TABLE -->
  <td valign="top"><?php include("scoretable.php"); ?></td>
  <!-- CHART -->
  <td align="center" valign="top">
    <table>
      <tr>
        <td bgcolor="lightgrey"><?php include("chart.php"); ?></td>
      </tr>
      <tr><td align="center" bgcolor="lightgrey"><h3>Score system</h3></td></tr>
      <tr>
        <td align="center" bgcolor="lightgrey">
          <table>
          <tr>
            <td>Critical:</td>
            <td>15 points</td>
          </tr>
          <tr>
            <td>High:</td>
            <td>10 points</td>
          </tr>
          <tr>
            <td>Medium:</td>
            <td>5 points</td>
          </tr>
          <tr>
            <td>Low:</td>
            <td>1 point</td>
          </tr>
          </table>
        </td>
      </tr>
    </table>
  </td>
  <!-- LIST OF BUGS TABLE -->
  <td valign="top"><?php include("list_bugs.php"); ?></td>
</tr>
</table>
</form>

</body>
</html>
