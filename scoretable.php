<?php
  include_once("checklogged.php");
  if( !checklogged() ) {
    header("Location: index.php");
    exit;
  }
  ?>

<?php
// QUERY
  require_once("Db.php");
  $db_handle = new Db(); 
  $query = "
    SELECT
      USR.name AS usr_name,
      SUM(SEV.sev_points) AS user_score
    FROM Bug BG
    LEFT JOIN Severity SEV
      ON BG.sev_id = SEV.id
    JOIN User USR
      ON USR.id = BG.user_id
    GROUP BY USR.name
    ORDER BY user_score DESC;";

  $bug_count = $db_handle->select($query); 
?>

<table>
<tr>
  <th align="left">User</th>
  <th align="left">Score</th>
</tr>
  <?php
    foreach ( $bug_count as $bug_value ) {
      echo '<tr>';
      echo '<td>'.$bug_value['usr_name'].'</td>';
      echo '<td>'.$bug_value['user_score'].'</td>';
      echo '</tr>';
    }
  ?>
</tr>
</table>
