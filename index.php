<?php
  include_once("checklogged.php");
  session_start();
  if( checklogged() ) {
    header("Location: main.php");
    exit;
  }
  ?>

<?php
if( count($_POST) > 0 ) {
  // We have chosen the user
  session_start();
  $_SESSION['userid'] = $_POST['userid'];
  header('Location: main.php');
} else {
  // FIRST RUN
  require_once("Db.php");
  $db_handle = new Db();
  $query = "SELECT id, name FROM User ORDER BY name";
  $users = $db_handle->select($query);
  $count = count($users);

  if ( ($count > 0) && is_array($users) ) {
    // we have a list of users  
  } else {
    echo "Something went wrong but I won't tell what.";
  }   
}

?>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>BugRace</title>
  </head>
  
<body>

<center>
<div style="width:70%;">
<h2>BugRace</h2>
<img src="img/bugrace.svg"></img>
<hr>
<h4>So, tell me who you are...</h4>
<!-- BUTTONS -->
<div id="User">
  <form method="POST" action="">
  <?php
    // The following is not entirely clear, mixing content with php
    // Replace with client-side javascript
    foreach ( $users as $user ) {
      echo '<button type="submit" name="userid" value="'.$user['id'].'">'.$user['name']."</button>";
    }
    ?> 
  </form>
</div>

Not a member yet? <br>
Write to konrad.pisarczyk@redstor.com to request a login.
</center>

<!-- END of center div -->
</div>

</body>
</html>
