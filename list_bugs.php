<?php
  include_once("checklogged.php");
  if( !checklogged() ) {
    header("Location: index.php");
    exit;
  }
  ?>

<?php
// QUERY
  require_once("Db.php");
  $db_handle = new Db(); 
  $query = "
    SELECT
      user_id,
      bug_no,
      bug_desc,
      SEV.sev_name AS sev_name,
      USR.name AS usr_name
    FROM Bug BG
    JOIN Severity SEV
      ON BG.sev_id = SEV.id
    JOIN User USR
      ON USR.id = BG.user_id
    ORDER BY bug_datetime DESC;";
  $bugs = $db_handle->select($query); 
?>

<table>
<tr>
<!-- HEADERS -->
  <input type="hidden" name="user_id" value="<?=$user_id?>">
  <th align="left">User</th>
  <th align="left">Severity</th>
  <th align="left">Bug ref</th>
  <th align="left">Description</th>
</tr>
  
<tr>
<!-- SUBMIT -->
  <td></td>
  <td align="right"">
    <select name="severity">
      <option value="1">Critical</option>
      <option value="2">High</option>
      <option value="3">Medium</option>
      <option value="4">Low</option>
    </select>
  </td>
  <td><input type="number" style="width: 5em;" name="bug_no" placeholder="5" size="5" required></td>
  
  <td><input type="text" style="width: 100%;" id="bug_desc" name="bug_desc" placeholder="Description"></td>

  <td><input type="submit" value="Submit"></td>
</tr>

<?php
  // To avoid mixing html content with php
  // perhaps move the below to javascript
  foreach ( $bugs as $bug ) {
    echo '<tr>';
    echo '<td>'.$bug['usr_name'].'</td>';
    echo '<td>'.$bug['sev_name'].'</td>';
    echo '<td>'.$bug['bug_no'].'</td>';
    echo '<td>'.$bug['bug_desc'].'</td>';
    echo '</tr>';
  }
  ?>
</table>
