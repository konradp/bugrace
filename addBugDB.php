<?php
  include_once("checklogged.php");
  session_start();
  if( !checklogged() ) {
    header("Location: index.php");
    exit;
  }
  ?>

<?php
  include_once("Db.php");
  $db = new Db();    

  // Fields for the database come from the preceeding form and the session
  // Ensure values are quoted to avoid breakages (sql injection)
  $userid = $_SESSION['userid'];
  $user_id = $_POST['user_id'];
  $bug_datetime = $db -> quote(date("Y-m-d H:i:s"));
  $bug_no = $db -> quote($_POST['bug_no']);
  $bug_desc = $db -> quote($_POST['bug_desc']);
  $severity = $_POST['severity'];
    
  // Insert values into the database
  $query = "
    INSERT INTO Bug
      (id, bug_datetime, user_id, bug_no, bug_desc, sev_id)
    VALUES 
      (DEFAULT,".$bug_datetime.",".$user_id.",".$bug_no.",".$bug_desc.",".$severity.");";
  $db->query($query);

  // Once bug added, return to the main document
  header("Location: main.php");
?>
