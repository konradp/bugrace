<?php
  session_start();
  session_destroy();
  ?>
<center>
You have been logged out.<br>
You can return to login screen to login again if you wish.
</center>

<?php
  // The above message currently does not show
  // As an option, remove the redirect below to show it
  header("Location: index.php");
  ?>
