<script src="Chart.js"></script>

<?php
  include_once("checklogged.php");
  if( !checklogged() ) {
    header("Location: index.php");
    exit;
  }
  ?>

<?php
// QUERY
  require_once("Db.php");
  $db_handle = new Db(); 
  $query = "
    SELECT
      USR.name AS usr_name,
      user_id,
      bug_no,
      SUM(SEV.sev_points) AS user_score
    FROM Bug BG
    JOIN Severity SEV
      ON BG.sev_id = SEV.id
    JOIN User USR
      ON USR.id = BG.user_id
    GROUP BY USR.name
    ORDER BY user_score DESC;";

  $bug_count = $db_handle->select($query); 
?>
<?php
  // Convert sql result to javascript array of options for the chart
  $data = [];
  $tempArray = [];
  foreach ( $bug_count as $bug_value ) {
    $tempArray['value'] = $bug_value['user_score'];
    $tempArray['color'] = "purple";
    $tempArray['highlight'] = "#FF5A5E";
    $tempArray['label'] = $bug_value['usr_name'];
    array_push($data, $tempArray);
  }
  ?>

<canvas id="myChart" width="200" height="200"></canvas>

<script>
var data = <?=json_encode($data)?>;
var options = { animation: false };
var ctx = document.getElementById("myChart").getContext("2d");
var myNewChart = new Chart(ctx).Pie(data, options);
</script>
